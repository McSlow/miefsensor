#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include "DFRobot_RGBLCD.h"
#include "SparkFun_SCD30_Arduino_Library.h"
#define rote_led D3
#define blaue_led D4

//am Wemos D1 klebt der i2c bus an D1 und D2
const int sdaPin = D2;
const int sclPin = D1;

//Instanziiere den/die SCD30 CO2 Sensoren
SCD30 airSensor1;

//Initialisiere das Display
DFRobot_RGBLCD lcd(16,2);  //16 characters and 2 lines of show

//variablen
uint16_t co2 = 0;
float temp = 0 ;
float hum = 0;
uint8_t red = 50;
uint8_t green = 100;
uint8_t blue = 150;
double hue, saturation, lighting, value;

void co2color(int co2){
  if(co2 < 700){
    lcd.setRGB(0,255, 0);   //gruen 
  }
  else if (co2 >= 700 && co2 < 1000)
  {
    lcd.setRGB(255,255, 0); //gelb
  }
  else if (co2 >= 1000 && co2 < 3000)
  {
    lcd.setRGB(255, 157, 0); //orange
  }
  else{
    lcd.setRGB(255, 0, 0); //rot
  }

}

void setup() {
  // put your setup code here, to run once:
  pinMode(rote_led, OUTPUT);
  pinMode(D3, OUTPUT);
  Serial.begin(115200); 
  Wire.begin(sdaPin, sclPin);
  if (Wire.status() != I2C_OK) Serial.println("Something wrong with I2C");
  if (airSensor1.begin(Wire) == false) //Pass the Wire port to the .begin() function
  {
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }
  lcd.init();
  lcd.setRGB(0, 0, 255);
  lcd.setCursor(0,0);
  lcd.print("MiefSensor");
  lcd.setCursor(0,1);
  lcd.print("Version 1.0");
  delay(2000);
  lcd.clear();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (airSensor1.dataAvailable())
  {
    //Frage den Sensor ab
    co2 = airSensor1.getCO2();
    temp = (airSensor1.getTemperature() - 4 );
    hum = airSensor1.getHumidity();

  }

  //setze displayfarbe
  co2color(co2);

  //zeige wert auf Display
  lcd.setCursor(0,0);
  lcd.printf("Temp: %.1f C", temp);
  lcd.setCursor(0,1);
  lcd.printf("H: %.1f|%d ppm", hum, int(co2));
  if(co2 > 3000){
    //mief
    digitalWrite(rote_led, HIGH);

  }
  else{
    //kein mief mehr
    digitalWrite(rote_led, LOW);
  }
  delay(5000);
  lcd.clear();
}


